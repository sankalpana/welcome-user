import React, { Component } from "react";
import "./App.css";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Display from "./screens/Display";
import DisplayInput from "./screens/DisplayInput";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputVal: "User"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ inputVal: event.target.value });
  }
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route
              path="/input"
              render={props => (
                <DisplayInput handleChange={this.handleChange} />
              )}
            ></Route>
            <Route
              path="/"
              render={props => <Display inputVal={this.state.inputVal} />}
            ></Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

const style = {
  container: {
    padding: 1,
    fontSize: 18,
    background: "#222",
    color: "#aaa"
  }
};

export default App;
