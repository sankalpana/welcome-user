import React from "react";

var TxtRotate = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = "";
  this.tick();
  this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">' + this.txt + "</span>";

  var that = this;
  var delta = 300 - Math.random() * 100;

  if (this.isDeleting) {
    delta /= 2;
  }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === "") {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  var elements = document.getElementsByClassName("txt-rotate");
  for (var i = 0; i < elements.length; i++) {
    var toRotate = elements[i].getAttribute("data-rotate");
    console.log(localStorage.getItem("data"));
    var period = elements[i].getAttribute("data-period");
    if (toRotate) {
      new TxtRotate(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  // var css = document.createElement("style");
  // css.type = "text/css";
  // css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
  // document.body.appendChild(css);
};

function Display(props) {
  const data = [
    "saman",
    "kamal",
    "nuwan",
    "sankalpana",
    "rajith",
    "saman",
    "kamal",
    "nuwan",
    "sankalpana",
    "rajith",
    "saman",
    "kamal",
    "nuwan",
    "sankalpana",
    "rajith",
    "saman",
    "kamal",
    "nuwan",
    "sankalpana",
    "rajith",
    "saman",
    "kamal",
    "nuwan",
    "sankalpana",
    "rajith"
  ];
  console.log(data[1]);

  let val = props.value;
  return (
    <div className="absolute-center">
      <link
        href="https://fonts.googleapis.com/css?family=Raleway:200,100,400"
        rel="stylesheet"
        type="text/css"
      />
      <h2></h2>
      <h1>
        <span>Welcome,{data[val]} </span>
        <span
          className="txt-rotate"
          data-period="2000"
          // data-rotate='[ "nerdy.", "simple.", "pure JS.", "pretty.", "fun!" ]'
        ></span>
      </h1>
      <span>===================================</span>
      <h1>
        <span>Welcome, </span>
        <span
          className="txt-rotate"
          data-period="2000"
          data-rotate='[ "Saman.", "Kamal.", "Shiran", "Sankalpana", "Coorey" ]'
        ></span>
      </h1>
    </div>
  );
}

export default Display;
