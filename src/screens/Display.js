import React, { Component } from "react";

class Display extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputVal: props.inputVal
    };
    console.log(props);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ inputVal: nextProps.inputVal });
  }

  render() {
    return <h1>Welcome,{this.state.inputVal} </h1>;
  }
}

export default Display;
